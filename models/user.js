var mongo = require("mongoose");

var mongopass = require("passport-local-mongoose");
var schema = new mongo.Schema({
local: {
username: String,
password: String
},
google:{
	id:String,
	token:String,
	email:String,
	name:String
},
linkedin:{
	id:String,
	token:String,
	email:String,
	name:String
},
github:{
	id:String,
	token:String,
	email:String,
	name:String
}
});

schema.plugin(mongopass);

module.exports = new mongo.model("User", schema);
