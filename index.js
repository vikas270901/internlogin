var express=require("express");
var mongo = require("mongoose");
var app = express();
var passport = require("passport");
var parser = require("body-parser");
var User = require("./models/user");
var LocalStrategy = require("passport-local");
var mongopass = require("passport-local-mongoose");
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var LinkedInStrategy = require('passport-linkedin-oauth2').Strategy;
var GitHubStrategy = require('passport-github').Strategy;
app.set("view engine", "ejs");
app.use(parser.urlencoded({extended:true}));
app.use(require("express-session")({secret: "This is my secret", resave: false, saveUninitialized: false }));
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
//passport.serializeUser(User.serializeUser());
//passport.deserializeUser(User.deserializeUser());
passport.serializeUser(function(user, done){
	done(null, user.id);
});
passport.deserializeUser(function(id, done){
	User.findById(id, function(err, user){
	done(err, user);		
	});
});

mongo.connect("mongodb://localhost:27017/authn_1", { useNewUrlParser: true });

passport.use(new LinkedInStrategy({
    clientID:  "81wca1tcoqox66",
    clientSecret: "ACdRiooFhLS6mnp3",
    callbackURL: "http://localhost:3000/auth/linkedin/callback",
    scope: ['r_emailaddress', 'r_basicprofile'],
  }, function(accessToken, refreshToken, profile, done) {
	process.nextTick(function(){
       	User.findOne({'google.id':profile.id}, function(err, user){
       		if(err) return done(err);
       		if(user) return done(null, user);
       		else {
       			console.log(profile);
       			var newUser = new User();
       			newUser.linkedin.id = profile.id;
       			newUser.linkedin.token = accessToken;
       			newUser.linkedin.name = profile.name.givenName + ' ' + profile.name.familyName;
       			newUser.linkedin.email = profile.displayName;//profile.emails[0].value;
       			newUser.save(function(err){
       				if(err) throw err;
       				return done(null, newUser);
       			});
       		}
       	})
    });
    // User.findOrCreate(function(err, user) {
    //   if (err) { return done(err); }
    //   done(null, user);
    // });
  }
));

app.get('/auth/linkedin', passport.authenticate('linkedin', { scope: ['r_emailaddress', 'r_basicprofile'] }));
app.get('/auth/linkedin/callback', passport.authenticate('linkedin', { successRedirect: '/secret',failureRedirect: '/login' }));


passport.use(new GitHubStrategy({
    clientID:  "e3aaac295f9993d92166",
    clientSecret: "2c60f7ca136e986c697f4710e114c6d3b9498ba7",
    callbackURL: "http://localhost:3000/auth/github/callback"
  }, function(accessToken, refreshToken, profile, done) {
	process.nextTick(function(){
       	User.findOne({'github.id':profile.id}, function(err, user){
       		if(err) return done(err);
       		if(user) return done(null, user);
       		else {
       			console.log(profile);
       			var newUser = new User();
       			newUser.github.id = profile.id;
       			newUser.github.token = accessToken;
       			newUser.github.name = profile.name.givenName + ' ' + profile.name.familyName;
       			newUser.github.email = profile.displayName;//profile.emails[0].value;
       			newUser.save(function(err){
       				if(err) throw err;
       				return done(null, newUser);
       			});
       		}
       	})
    });
    // User.findOrCreate(function(err, user) {
    //   if (err) { return done(err); }
    //   done(null, user);
    // });
  }
));

app.get('/auth/github', passport.authenticate('github', { scope: 'email' }));
app.get('/auth/github/callback', passport.authenticate('github', { successRedirect: '/secret',failureRedirect: '/login' }));

passport.use(new GoogleStrategy({
    clientID: '1063071766066-trrsfiqmv6d13jfdrj8uim0vb6s249f3.apps.googleusercontent.com',
    clientSecret: 'iawvMsz7UxEfBajbfYbMVR4q',
    callbackURL: "http://localhost:3000/auth/google/callback"
  },
  function(accessToken, refreshToken, profile, done) {
       process.nextTick(function(){
       	User.findOne({'google.id':profile.id}, function(err, user){
       		if(err) return done(err);
       		if(user) return done(null, user);
       		else {
       			console.log(profile);
       			var newUser = new User();
       			newUser.google.id = profile.id;
       			newUser.google.token = accessToken;
       			newUser.google.name = profile.displayName;
       			newUser.google.email = profile.emails[0].value;
       			newUser.save(function(err){
       				if(err) throw err;
       				return done(null, newUser);
       			});
       		}
       	})
    })
}));

app.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email'] })); //['https://www.googleapis.com/auth/plus.login']
app.get('/auth/google/callback', passport.authenticate('google', { successRedirect: "/secret", failureRedirect: "/login" }), 
	function(req, res){
});

app.get("/", function(req, res){
	res.render("home");
});

app.get("/secret", islogged, function(req, res){
	res.render("secret");
});

app.get("/register", function(req, res){
	res.render("register");
});

app.post("/register", function(req, res){
	User.register(new User({username:req.body.username}), req.body.password, function(err, user){
		if(err) {console.log(err);
			return res.render("register");
		}
		passport.authenticate("local")(req, res, function(){
					res.redirect("/secret");
});});
});

app.get("/login", function(req, res){
	res.render("login");
});

app.post("/login", passport.authenticate("local", {
	successRedirect: "/secret",
	failureRedirect: "/login"
}), function(req, res){
});

app.get("/logout", function(req, res){
	req.logout();
	res.redirect("/");
});

function islogged(req, res, next){
	if(req.isAuthenticated()){
		return next();
	}
	res.redirect("/login");
}

app.listen(3000, process.env.IP, function(){
	console.log("Server Started at 3000...");
})


//var passpot = require('passport')
//   , linkedinStrategy = require('passport-linkedin').Strategy;

// passpot.use(new linkedinStrategy({
//     clientID:  "759841961079000",
//     clientSecret: "",
//     callbackURL: "http://localhost:3000/auth/linkedin/callback"
//   }, function(accessToken, refreshToken, profile, done) {
// 	process.nextTick(function(){
//        	User.findOne({'google.id':profile.id}, function(err, user){
//        		if(err) return done(err);
//        		if(user) return done(null, user);
//        		else {
//        			var newUser = new User();
//        			newUser.google.id = profile.id;
//        			newUser.google.token = accessToken;
//        			newUser.google.name = profile.name.givenName + ' ' + profile.name.familyName;
//        			newUser.google.email = profile.emails[0].value;

//        			newUser.save(function(err){
//        				if(err) throw err;
//        				return done(null, newUser);
//        			});
//        		}
//        	})
//     });
//     // User.findOrCreate(function(err, user) {
//     //   if (err) { return done(err); }
//     //   done(null, user);
//     // });
//   }
// ));

// app.get('/auth/linkedin', passport.authenticate('linkedin', { scope: ['email'] }));
// app.get('/auth/linkedin/callback', passport.authenticate('linkedin', { successRedirect: '/',failureRedirect: '/login' }));
